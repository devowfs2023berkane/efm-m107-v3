<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnonceVelo extends Model
{
    use HasFactory;

    protected $primaryKey = 'idAnnonce';

    protected $fillable = [
        'titre',
        'description',
        'datePublication',
        'referenceVelo',
    ];

    public function velo()
    {
        return $this->belongsTo(VeloElectrique::class, 'referenceVelo', 'referenceVelo');
    }

    public function ventes()
    {
        return $this->hasMany(Vente::class, 'idAnnonce', 'idAnnonce');
    }
}
