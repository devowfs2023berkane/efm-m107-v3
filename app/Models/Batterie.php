<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batterie extends Model
{
    use HasFactory;

    protected $primaryKey = 'idBat';

    protected $fillable = [
        'marque',
        'dureeVie',
        'prix',
        'puissance',
    ];

    public function velos()
    {
        return $this->hasMany(VeloElectrique::class, 'idBat', 'idBat');
    }
}
