<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VeloElectrique extends Model
{
    use HasFactory;

    protected $primaryKey = 'referenceVelo';

    protected $fillable = [
        'marque',
        'prix',
        'idBat',
    ];

    public function batterie()
    {
        return $this->belongsTo(Batterie::class, 'idBat', 'idBat');
    }

    public function annonces()
    {
        return $this->hasMany(AnnonceVelo::class, 'referenceVelo', 'referenceVelo');
    }
}
