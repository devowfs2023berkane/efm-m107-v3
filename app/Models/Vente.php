<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vente extends Model
{
    use HasFactory;

    protected $primaryKey = 'idVente';

    protected $fillable = [
        'dateVente',
        'annonce_velo_id',
    ];

    public function annonce()
    {
        return $this->belongsTo(AnnonceVelo::class);
    }
}
