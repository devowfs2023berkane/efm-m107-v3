<?php

namespace App\Http\Controllers;

use App\Models\Batterie;
use App\Models\VeloElectrique;
use Illuminate\Http\Request;

class VeloElectriqueController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $batteries = Batterie::all();
        $messages = '[]';
        return view('velos.create', compact('batteries','messages'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'marque' => 'required',
            'prix' => 'required|integer',
            'idBat' => 'required|integer|exists:batteries,idBat',
        ]);
        VeloElectrique::create($validatedData);
        return redirect()->route('annonces.index')->with('message', 'Vélo électrique ajouté !');
    }

    /**
     * Display the specified resource.
     */
    public function show(VeloElectrique $veloElectrique)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(VeloElectrique $veloElectrique)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, VeloElectrique $veloElectrique)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(VeloElectrique $veloElectrique)
    {
        //
    }
}
