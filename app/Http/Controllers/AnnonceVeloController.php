<?php

namespace App\Http\Controllers;

use App\Models\AnnonceVelo;
use Illuminate\Http\Request;

class AnnonceVeloController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $annonces = AnnonceVelo::all()->load('ventes', 'velo');
//        dd($annonces);
        return view('annonces.index', compact('annonces'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('annonces.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AnnonceVelo $annonceVelo)
    {
        return view('annonces.show', compact('annonceVelo'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AnnonceVelo $annonceVelo)
    {
        return view('annonces.edit', compact('annonceVelo'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AnnonceVelo $annonceVelo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AnnonceVelo $annonceVelo)
    {
        $annonceVelo->delete();
        return redirect()->route('annonces.index')->with('message', 'Annonce supprimée !');
    }
}
