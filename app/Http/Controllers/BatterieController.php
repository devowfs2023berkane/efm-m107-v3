<?php

namespace App\Http\Controllers;

use App\Models\Batterie;
use Illuminate\Http\Request;

class BatterieController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
//        return view('batteries.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Batterie $batterie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Batterie $batterie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Batterie $batterie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Batterie $batterie)
    {
        //
    }
}
