<?php

use App\Models\VeloElectrique;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('annonce_velos', function (Blueprint $table) {
            $table->id('idAnnonce');
            $table->string('titre');
            $table->string('description');
            $table->date('datePublication');
            $table->unsignedBigInteger('referenceVelo');
            $table->foreign('referenceVelo')->references('referenceVelo')->on('velo_electriques');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('annonce_velos');
    }
};
