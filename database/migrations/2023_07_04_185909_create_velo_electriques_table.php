<?php

use App\Models\Batterie;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('velo_electriques', function (Blueprint $table) {
            $table->id('referenceVelo');
            $table->string('marque');
            $table->string('prix');
            $table->unsignedBigInteger('idBat');
            $table->foreign('idBat')->references('idBat')->on('batteries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('velo_electriques');
    }
};
