<?php

namespace Database\Factories;

use App\Models\AnnonceVelo;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vente>
 */
class VenteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "dateVente"=>$this->faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
            "idAnnonce"=>AnnonceVelo::all()->random()->idAnnonce,
        ];
    }
}
