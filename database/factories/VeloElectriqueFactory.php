<?php

namespace Database\Factories;

use App\Models\Batterie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\VeloElectrique>
 */
class VeloElectriqueFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "marque"=>$this->faker->word(),
            "prix"=>$this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
            "idBat"=> Batterie::all()->random()->idBat,
        ];
    }
}
