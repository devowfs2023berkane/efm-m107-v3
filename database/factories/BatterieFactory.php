<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Batterie>
 */
class BatterieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "marque" => $this->faker->word(),
            "dureeVie" => $this->faker->randomFloat(2, 0, 100),
            "prix" => $this->faker->randomFloat(2, 0, 100),
            "puissance" => $this->faker->randomFloat(2, 0, 100),
        ];
    }
}
