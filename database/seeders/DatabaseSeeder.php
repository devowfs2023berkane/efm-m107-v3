<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AnnonceVelo;
use App\Models\Batterie;
use App\Models\User;
use App\Models\VeloElectrique;
use App\Models\Vente;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();
        Batterie::factory(10)->create();
        VeloElectrique::factory(10)->create();
        AnnonceVelo::factory(10)->create();
        Vente::factory(10)->create();


        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
