# Examen Regional de Fin de Module
_Developpement digital 1er annee_ `M107` _2023_ v3

## Laravel: Breeze with Blade

## Etapes d'installation

### Pour Laravel

```bash
composer i 
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan storage:link
```

```bash
php artisan migrate
```

- `To generate fake data`

```bash
php artisan migrate:fresh --seed
```

```bash
php artisan serve 
```

### Pour Blade


```bash
npm i 
```

```bash
npm run dev
```
