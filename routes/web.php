<?php

use App\Http\Controllers\AnnonceVeloController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VeloElectriqueController;
use App\Models\AnnonceVelo;
use App\Models\VeloElectrique;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $nombreAnnonces = App\Models\AnnonceVelo::count();
    $chiffreAffaires = VeloElectrique::selectRaw('sum(prix) as chiffreAffaires')
        ->join('annonce_velos', 'velo_electriques.referenceVelo', '=', 'annonce_velos.referenceVelo')
        ->join('ventes', 'annonce_velos.idAnnonce', '=', 'ventes.idAnnonce')
        ->first()
        ->chiffreAffaires;
    return view('dashboard', compact('nombreAnnonces', 'chiffreAffaires'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('annonces', [AnnonceVeloController::class, 'index'])->name('annonces.index');
    Route::get('annonces/{annonceVelo}', [AnnonceVeloController::class, 'show'])->name('annonces.show');
    Route::get('annonces/{annonceVelo}/edit', [AnnonceVeloController::class, 'edit'])->name('annonces.edit');
    Route::put('annonces/{annonceVelo}', [AnnonceVeloController::class, 'update'])->name('annonces.update');
    Route::delete('annonces/{annonceVelo}', [AnnonceVeloController::class, 'destroy'])->name('annonces.destroy');

    Route::get('velos/create', [VeloElectriqueController::class, 'create'])->name('velos.create');
    Route::post('velos', [VeloElectriqueController::class, 'store'])->name('velos.store');
});

require __DIR__ . '/auth.php';
