<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>

</head>
<body class="antialiased">

<h1>Welcome</h1>


<x-nav-link href="{{ route('annonces.index') }}" >
    {{ __('Annonces') }}
</x-nav-link>

<x-nav-link href="{{ route('register') }}" class="mr-3">
    {{ __('Register') }}
</x-nav-link>

<x-nav-link href="{{ route('login') }}">
    {{ __('Login') }}
</x-nav-link>
</body>
</html>


