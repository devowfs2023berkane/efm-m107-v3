<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{--                    {{ __("") }}--}}
                    <p class="p-3">Nombre d'annonce <span class="bg-blue-600 p-1 rounded-lg text-gray-50">{{$nombreAnnonces}}</span></p>
                    <p class="p-3">Chiffre d'affaire total <span class="bg-orange-500 p-2 rounded-lg text-gray-50">{{ $chiffreAffaires }} DHs</span></p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
