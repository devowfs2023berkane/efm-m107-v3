<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h1 class="text-3xl text-center">Liste des annonces</h1>
{{--                    <x-success-message />--}}
{{--                    <x-danger-message />--}}
                    <div class="flex justify-end">
                        <x-primary-button class="bg-blue-600 hover:bg-blue-700 my-3 ">
{{--                        <a href="{{ route('annonces.create') }}">Ajouter nouveau annonce</a>--}}
                        <a href="#">Ajouter nouveau annonce</a>
                    </x-primary-button>
                    </div>
                    <table class="bg-orange-100 rounded-xl ">
                        <thead class="bg-orange-500 text-gray-50 rounded-t-md font-extrabold">
                        <tr>
                            <th>Identifiant</th>
                            <th>Titre</th>
                            <th>Description</th>
                            <th>Date de publication</th>
                            <th>Prix de velos de l'annonce</th>
                            <th>Marque de velo de l'annonce</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($annonces as $annonce)
                            <tr>
                                <td class="text-center font-bold">{{ $annonce->idAnnonce }}</td>
                                <td>{{ $annonce->titre }}</td>
                                <td>{{ $annonce->description }}</td>
                                <td>{{ $annonce->datePublication }}</td>
                                <td>{{ $annonce->velo->prix }}</td>
                                <td>{{ $annonce->velo->marque }}</td>
                                <td class="py-2">
                                    <x-primary-button class="bg-green-600 hover:bg-green-900">
                                        <a href="{{ route('annonces.show', $annonce->idAnnonce) }}">Voir</a>
                                    </x-primary-button>
                                    <x-primary-button class="bg-yellow-600 hover:bg-yellow-700 my-1">
                                        <a href="{{ route('annonces.edit', $annonce->idAnnonce) }}">Modifier</a>
                                    </x-primary-button>
                                    <form action="{{ route('annonces.destroy', $annonce->idAnnonce) }}" method="POST" onsubmit="return confirm('vous ete sure, supprimer cette annonce!!')">
                                        @csrf
                                        @method('DELETE')
{{--                                        <x-primary-button type="submit" class="bg-red-700 hover:bg-red-800">--}}
{{--                                            Supprimer--}}
{{--                                        </x-primary-button>--}}
                                        <x-danger-button type="submit" >
                                            Supprimer
                                        </x-danger-button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7">Aucune annonce</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
