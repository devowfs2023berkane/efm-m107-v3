<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{--                    {{ __("") }}--}}
                    <h1 class="text-3xl mt-5 mb-3">Ajouter Nouveau Velo </h1>

                    <form action="{{route('velos.store')}}" method="POST">
                        @csrf
                        @if($errors->any())
                            @foreach($errors->messages() as $msg)
                                <x-input-error :messages="$msg"/>
                            @endforeach
                        @endif
                        <x-text-input type="text" name="marque" value="{{old('marque')}}" placeholder="Marque de velo"/>
                        <x-text-input type="number" name="prix" value="{{old('prix')}}"  placeholder="Prix de velo"/>
                        <select name="idBat" id="">
                            <option value="" hidden selected>Choisi le batterie</option>
                            @foreach($batteries as $batterie)
                                <option value="{{$batterie->idBat}}" @selected(old('idBat')==$batterie->idBat)>{{$batterie->marque}}</option>
                            @endforeach
                        </select>
                        <x-primary-button type="submit" class="bg-green-600 hover:bg-green-900 py-3">
                            Ajouter
                        </x-primary-button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
